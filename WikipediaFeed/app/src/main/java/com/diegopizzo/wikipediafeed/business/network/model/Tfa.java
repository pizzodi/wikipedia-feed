package com.diegopizzo.wikipediafeed.business.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by diegopizzo on 25/10/2017.
 */

public class Tfa {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("displaytitle")
    @Expose
    private String displaytitle;
    @SerializedName("pageid")
    @Expose
    private int pageid;
    @SerializedName("extract")
    @Expose
    private String extract;
    @SerializedName("extract_html")
    @Expose
    private String extractHtml;
    @SerializedName("thumbnail")
    @Expose
    private Thumbnail thumbnail;
    @SerializedName("originalimage")
    @Expose
    private Originalimage originalimage;
    @SerializedName("lang")
    @Expose
    private String lang;
    @SerializedName("dir")
    @Expose
    private String dir;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;
    @SerializedName("description")
    @Expose
    private String description;

    public Originalimage getOriginalimage() {
        return originalimage;
    }

    public void setOriginalimage(final Originalimage originalimage) {
        this.originalimage = originalimage;
    }

    public Tfa withOriginalimage(final Originalimage originalimage) {
        this.originalimage = originalimage;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public Tfa withTitle(final String title) {
        this.title = title;
        return this;
    }

    public String getDisplaytitle() {
        return displaytitle;
    }

    public void setDisplaytitle(final String displaytitle) {
        this.displaytitle = displaytitle;
    }

    public Tfa withDisplaytitle(final String displaytitle) {
        this.displaytitle = displaytitle;
        return this;
    }

    public int getPageid() {
        return pageid;
    }

    public void setPageid(final int pageid) {
        this.pageid = pageid;
    }

    public Tfa withPageid(final int pageid) {
        this.pageid = pageid;
        return this;
    }

    public String getExtract() {
        return extract;
    }

    public void setExtract(final String extract) {
        this.extract = extract;
    }

    public Tfa withExtract(final String extract) {
        this.extract = extract;
        return this;
    }

    public String getExtractHtml() {
        return extractHtml;
    }

    public void setExtractHtml(final String extractHtml) {
        this.extractHtml = extractHtml;
    }

    public Tfa withExtractHtml(final String extractHtml) {
        this.extractHtml = extractHtml;
        return this;
    }

    public Thumbnail getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(final Thumbnail thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Tfa withThumbnail(final Thumbnail thumbnail) {
        this.thumbnail = thumbnail;
        return this;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(final String lang) {
        this.lang = lang;
    }

    public Tfa withLang(final String lang) {
        this.lang = lang;
        return this;
    }

    public String getDir() {
        return dir;
    }

    public void setDir(final String dir) {
        this.dir = dir;
    }

    public Tfa withDir(final String dir) {
        this.dir = dir;
        return this;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(final String timestamp) {
        this.timestamp = timestamp;
    }

    public Tfa withTimestamp(final String timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public Tfa withDescription(final String description) {
        this.description = description;
        return this;
    }

}
