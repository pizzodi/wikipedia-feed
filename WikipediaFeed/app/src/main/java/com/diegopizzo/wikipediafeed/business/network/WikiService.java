package com.diegopizzo.wikipediafeed.business.network;

import com.diegopizzo.wikipediafeed.business.network.model.WikiModel;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;

/**
 * Created by diegopizzo on 25/10/2017.
 */

public interface WikiService {

    String SERVICE_ENDPOINT = "https://en.wikipedia.org";

    @Headers({"Content-Type: application/json"})
    @GET("/api/rest_v1/feed/featured/{yyyy}/{mm}/{dd}")
    Observable<WikiModel> getWikiFeed(@Path("yyyy") String year, @Path("mm") String month, @Path("dd") String day);
}
