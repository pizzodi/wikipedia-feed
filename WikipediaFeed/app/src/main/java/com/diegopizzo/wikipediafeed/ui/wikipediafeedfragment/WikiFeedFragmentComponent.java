package com.diegopizzo.wikipediafeed.ui.wikipediafeedfragment;


import com.diegopizzo.wikipediafeed.config.dagger.ApplicationComponent;
import com.diegopizzo.wikipediafeed.config.dagger.FragmentScope;

import dagger.Component;

/**
 * Created by diegopizzo on 26/10/2017.
 */

@FragmentScope
@Component(dependencies = ApplicationComponent.class, modules = WikiFeedFragmentModule.class)
public interface WikiFeedFragmentComponent {

    void inject(WikiFeedFragment wikiFeedFragment);
}
