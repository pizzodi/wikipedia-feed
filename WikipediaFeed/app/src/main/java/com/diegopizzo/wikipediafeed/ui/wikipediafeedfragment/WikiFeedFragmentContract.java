package com.diegopizzo.wikipediafeed.ui.wikipediafeedfragment;

import com.diegopizzo.wikipediafeed.business.network.model.WikiModel;
import com.diegopizzo.wikipediafeed.config.mvp.MvpPresenter;
import com.diegopizzo.wikipediafeed.config.mvp.MvpView;

/**
 * Created by diegopizzo on 26/10/2017.
 */

public interface WikiFeedFragmentContract {

    interface View extends MvpView {
        void setComponents(WikiModel wikiModel);
    }

    interface Presenter extends MvpPresenter {
        void wikiFeedNews();
    }

}
